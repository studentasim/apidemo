package com.asimhussain.apidemo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asimhussain.apidemo.entity.Account;
import com.asimhussain.apidemo.entity.BalanceEvent;
import com.asimhussain.apidemo.error.AccountNotFoundException;
import com.asimhussain.apidemo.repository.AccountRepository;
import com.asimhussain.apidemo.repository.BalanceRepository;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Api(tags="Balance Event Controller")
public class BalanceEventController {
	
	@Autowired private BalanceRepository balanceRepository;
	@Autowired private AccountRepository accountRepository;
	
	 @GetMapping("/balance/{accountNumber}")
	 @ApiResponses(value = { 
		        @ApiResponse(responseCode = "200", description = "Latest account balance found"), 
		        @ApiResponse(responseCode = "404", description = "Account does not exist or resource not found"),
		        @ApiResponse(responseCode = "204", description = "No balance found.") }) 
	 public ResponseEntity<BalanceEvent> balance(@PathVariable Long accountNumber, 
			 @RequestParam(name = "pageNumber", required = true) Integer pageNumber, @RequestParam(name = "pageSize", required = true) Integer pageSize) throws AccountNotFoundException {
		 log.debug("inside BalanceEventController");
		 
		 Account account = new Account(accountNumber);
		 
		 // does accountNumber exist?
		 Example<Account> example = Example.of(account);
		 Optional<Account> actual = accountRepository.findOne(example);
		 if (actual.isEmpty()) {
			 throw new AccountNotFoundException();
		 }
		 
		 // retrieve the most recent balance
		 Pageable accountIdSortedByLastUpdateTimestampDesc = PageRequest.of(pageNumber, pageSize, Sort.by("lastUpdateTimestamp").descending());
		 Page<BalanceEvent> balance = balanceRepository.findByAccount(account,accountIdSortedByLastUpdateTimestampDesc);
		 Optional<BalanceEvent> firstBalance = balance.get().findFirst();
		 if (firstBalance.isPresent())
			 return new ResponseEntity<BalanceEvent>(firstBalance.get(),HttpStatus.OK);
		 // return a 204 NO CONTENT otherwise
		 return ResponseEntity.noContent().build();
    }
}