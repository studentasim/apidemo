package com.asimhussain.apidemo.support;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Optional;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class GenerateRandomFactory {
	
	private static Random rand = new Random();
	private static NumberFormat df = NumberFormat.getInstance();
	static {
		df.setRoundingMode(RoundingMode.FLOOR);
		df.setMinimumFractionDigits(0);
		df.setMaximumFractionDigits(2);
	}
	
	public Optional<Number> getRandomNumber(RandomNumberType numberTye) throws ParseException {
		
		switch (numberTye) {
		case BIG_DECIMAL: {
			BigDecimal randomBigDecimal = generateRandomBigDecimalFromRange(new BigDecimal(-100.21).setScale(2, BigDecimal.ROUND_HALF_UP),
		    new BigDecimal(1221.28).setScale(2, BigDecimal.ROUND_HALF_UP));
			return Optional.of(df.parse(randomBigDecimal.toPlainString()));
		} case LONG: {
			//return Optional.of(df.parse(new Long(rand.nextLong()).toString(),parsePosition));
			return Optional.of(df.parse(Long.toString(Math.abs(rand.nextLong()))));
		}
		default:
			return null;
		}
	}
	
	public TransactionType getTransactionType() {
		boolean bool = rand.nextBoolean();
		if (bool==Boolean.TRUE) {
			return TransactionType.DEPOSIT;
		} else {
			return TransactionType.WITHDRAW;
		}
	}
	
	public BigDecimal generateRandomBigDecimalFromRange(BigDecimal min, BigDecimal max) {
	    BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
	    return randomBigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP);
	}
}