package com.asimhussain.apidemo.repository;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.asimhussain.apidemo.entity.Account;
import com.asimhussain.apidemo.entity.TransactionEvent;
import com.asimhussain.apidemo.support.TransactionType;

@Transactional
public interface TransactionRepository extends JpaRepository<TransactionEvent, Long> {
	
	List<TransactionEvent> findByTransactionTsBetween(LocalDateTime from, LocalDateTime to);
	List<TransactionEvent> findByAccountAndTransactionTsBetweenOrderByTransactionTsDesc(Account account, LocalDateTime from, LocalDateTime to);
	List<TransactionEvent> findByAccountAndTransactionTsBetweenAndTransactionTypeOrderByTransactionTsDesc(Account account, LocalDateTime from, LocalDateTime to, TransactionType transactionType);
	Page<TransactionEvent> findByAccountAndTransactionTsBetween(Account accountId, LocalDateTime from, LocalDateTime to, Pageable pagable);
	Page<TransactionEvent> findByAccountAndTransactionTsBetweenAndTransactionType(Account accountId, LocalDateTime from, LocalDateTime to, TransactionType transactionType, Pageable pagable);
	
}
