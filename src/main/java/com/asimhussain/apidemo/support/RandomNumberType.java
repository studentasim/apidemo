package com.asimhussain.apidemo.support;

public enum RandomNumberType {
	BIG_DECIMAL,
	LONG
}