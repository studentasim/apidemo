package com.asimhussain.apidemo.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.asimhussain.apidemo.entity.Account;
import com.asimhussain.apidemo.entity.BalanceEvent;

@Transactional
public interface BalanceRepository extends JpaRepository<BalanceEvent, Long> {
	
	BalanceEvent findFirstByAccountIdOrderByLastUpdateTimestampDesc(Long accountId);
	Page<BalanceEvent> findByAccount(Long accountId,Pageable pagable);
	Page<BalanceEvent> findByAccount(Account accountId,Pageable pagable);
	List<BalanceEvent> findByAccountIdOrderByLastUpdateTimestampAsc(Long accountId);
	BalanceEvent findTop1ByOrderByLastUpdateTimestampDesc();
	
}
