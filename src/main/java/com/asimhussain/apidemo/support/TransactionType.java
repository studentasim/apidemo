package com.asimhussain.apidemo.support;

public enum TransactionType {

	DEPOSIT("DEPOSIT"),
	WITHDRAW("WITHDRAW");
	
    private String transactionType;
	 
	TransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
 
    public String getTransactionType() {
        return transactionType;
    }
	
}