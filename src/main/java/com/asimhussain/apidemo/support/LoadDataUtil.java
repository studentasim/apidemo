package com.asimhussain.apidemo.support;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.asimhussain.apidemo.entity.Account;
import com.asimhussain.apidemo.entity.BalanceEvent;
import com.asimhussain.apidemo.entity.TransactionEvent;
import com.asimhussain.apidemo.repository.AccountRepository;

@Component
public class LoadDataUtil {

	@Value("${data.people.limit}") private Integer PEOPLE_LIMIT;
	@Value("${data.per.person}") private Integer PER_PERSON;
	@Autowired private GenerateRandomFactory generateRandomFactory;
	@Autowired private AccountRepository accountRepository;
	private static Random rand = new Random();

	public void loadDummyData() throws ParseException {

		Long accountNumberCommon = null;

		for (int i = 0; i < PEOPLE_LIMIT; i++) {
			
			// for each person, create an Account
			Account account = new Account();

			Optional<Number> accountNumber = generateRandomFactory.getRandomNumber(RandomNumberType.LONG);
			if (accountNumber.isPresent()) {
				accountNumberCommon = accountNumber.get().longValue();
			}
			
			// set account primary key
			account.setId(accountNumberCommon);
			
			Set<BalanceEvent> balances = new HashSet<BalanceEvent>();
			Set<TransactionEvent> transactions = new HashSet<TransactionEvent>();
			account.setBalances(balances);
			account.setTransactions(transactions);
			
			Instant randomInstant = null;

			for (int j = 0; j < PER_PERSON; j++) {

				// ***** create BALANCE *****
				BalanceEvent balanceEvent = new BalanceEvent();
					balanceEvent.setAccount(account);
					balanceEvent.setBalance(BigDecimal.valueOf(getRandomNumber().longValue()));
				randomInstant = randomDate();
				balanceEvent.setLastUpdateTimestamp(LocalDateTime.ofInstant(randomInstant, ZoneOffset.UTC));
				
				// save Balance in set
				balances.add(balanceEvent);

				// ***** create Transaction *****
				TransactionEvent transactionEvent = new TransactionEvent();
				transactionEvent.setAccount(account);

				// randomize the date again
				randomInstant = randomDate();
				transactionEvent.setTransactionTs(LocalDateTime.ofInstant(randomInstant, ZoneOffset.UTC));
				transactionEvent.setTransactionType(generateRandomFactory.getTransactionType());
				
				// generate an amount
				transactionEvent.setAmount(BigDecimal.valueOf(getRandomNumber().longValue()));
				
				// save Transaction in set
				transactions.add(transactionEvent);
			}
			// save TOTAL item details for transactions and balances PER account
			accountRepository.save(account);
		}
	}

	public Instant randomDate() {
		Integer random = 0 + rand.nextInt(730 - 0 + 1);
		Instant randomDateWithinLast2Years = Instant.now().minus(Duration.ofDays(Long.valueOf(random.toString())));
		return randomDateWithinLast2Years;
	}
	
	public Number getRandomNumber() throws ParseException {
		Optional<Number> number = generateRandomFactory.getRandomNumber(RandomNumberType.BIG_DECIMAL);
		if (number.isPresent()) {
			return number.get();
		} else {
			return 0;
		}
	}
}