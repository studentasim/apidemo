package com.asimhussain.apidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsimHussainAPIDemo {

	public static void main(String[] args) {
		SpringApplication.run(AsimHussainAPIDemo.class, args);
	}
}