package com.asimhussain.apidemo.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalErrorHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value 
		      = { IllegalArgumentException.class, IllegalStateException.class })
	public ResponseEntity<Object> handleConflict(RuntimeException ex) {
		return new ResponseEntity<Object>("Oops. Something has gone wrong.",HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = { AccountNotFoundException.class })
	public ResponseEntity<Object> handleAccount(AccountNotFoundException ex) {
		return new ResponseEntity<Object>(ex.getMessage(),HttpStatus.NOT_FOUND);
	}
	
}