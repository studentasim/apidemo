package com.asimhussain.apidemo.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Account {

	@NonNull @NotNull @Id private Long id;
	@OneToMany(mappedBy = "account", fetch = FetchType.LAZY,
			  cascade = {CascadeType.PERSIST, CascadeType.MERGE}) private Set<BalanceEvent> balances;
	@OneToMany(mappedBy = "account", fetch = FetchType.LAZY,
			  cascade = {CascadeType.PERSIST, CascadeType.MERGE}) private Set<TransactionEvent> transactions;

}
