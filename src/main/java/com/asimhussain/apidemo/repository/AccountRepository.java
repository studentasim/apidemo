package com.asimhussain.apidemo.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asimhussain.apidemo.entity.Account;

@Transactional
public interface AccountRepository extends JpaRepository<Account, Long> {}
