package com.asimhussain.apidemo.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asimhussain.apidemo.entity.Account;
import com.asimhussain.apidemo.entity.TransactionEvent;
import com.asimhussain.apidemo.error.AccountNotFoundException;
import com.asimhussain.apidemo.repository.AccountRepository;
import com.asimhussain.apidemo.repository.TransactionRepository;
import com.asimhussain.apidemo.support.TransactionType;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Api(tags = "Transaction Event Controller")
public class TransactionEventController {

	@Autowired private TransactionRepository transactionRepository;
	@Autowired private AccountRepository accountRepository;

	@GetMapping("/transaction/{accountNumber}")
	 @ApiResponses(value = { 
		        @ApiResponse(responseCode = "200", description = "Transactions provided between date(s) specified."), 
		        @ApiResponse(responseCode = "404", description = "Account does not exist or resource not found"), }) 
	public ResponseEntity<List<TransactionEvent>> transaction(@PathVariable Long accountNumber,
			@RequestParam(name = "dateRangeFrom", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateRangeFrom,
			@RequestParam(name = "dateRangeTo", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateRangeTo,
			@RequestParam(name = "type", required = false) TransactionType transactionType,
			@RequestParam(name = "pageNumber", required = true) Integer pageNumber,
			@RequestParam(name = "pageSize", required = true) Integer pageSize) throws AccountNotFoundException {

		// set up an account object
		Account account = new Account(accountNumber);

		// does accountNumber exist?
		Example<Account> example = Example.of(account);
		Optional<Account> actual = accountRepository.findOne(example);
		if (actual.isEmpty()) {
			log.debug("the account is not currently in the db");
			throw new AccountNotFoundException();
		}

		List<TransactionEvent> transactions;

		// create sort for ASC and DESC and pagination
		Pageable transactionEventsSortedByTransactionTsDesc = PageRequest.of(pageNumber, pageSize,
				Sort.by("transactionTs").descending());
		Pageable transactionEventsSortedByTransactionTsAsc = PageRequest.of(pageNumber, pageSize,
				Sort.by("transactionTs").ascending());

		// get transactions between the two provided dates
		if (dateRangeFrom != null && dateRangeTo != null) {
			LocalDateTime dateFrom = LocalDateTime.of(dateRangeFrom, LocalTime.MIDNIGHT);
			LocalDateTime dateEnd = LocalDateTime.of(dateRangeTo, LocalTime.MAX);

			// if TransactionType was specified
			if (transactionType != null) {
				Page<TransactionEvent> transactionz = transactionRepository
						.findByAccountAndTransactionTsBetweenAndTransactionType(account, dateFrom, dateEnd,
								transactionType, transactionEventsSortedByTransactionTsAsc);
				transactions = transactionz.getContent();

			} else {
				Page<TransactionEvent> transactionz = transactionRepository.findByAccountAndTransactionTsBetween(
						account, dateFrom, dateEnd, transactionEventsSortedByTransactionTsAsc);
				transactions = transactionz.getContent();
			}
		} else {

			// convert 'defaultDate' to LocalDateTime type
			LocalDateTime defaultDateTime = dateRangeFrom != null ? dateRangeFrom.atStartOfDay() : LocalDateTime.now();

			// get Now (default value)
			LocalDateTime now = LocalDateTime.now();

			// is this just for today?
			if (now.toLocalDate().isEqual(defaultDateTime.toLocalDate())) {

				// get all transactions for just today
				LocalDateTime startOfToday = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
				LocalDateTime endOfToday = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);

				// if TransactionType was specified
				if (transactionType != null) {
					Page<TransactionEvent> transactionz = transactionRepository
							.findByAccountAndTransactionTsBetweenAndTransactionType(account, startOfToday, endOfToday,
									transactionType, transactionEventsSortedByTransactionTsAsc);
					transactions = transactionz.getContent();

				} else {
					Page<TransactionEvent> transactionz = transactionRepository.findByAccountAndTransactionTsBetween(
							account, startOfToday, endOfToday, transactionEventsSortedByTransactionTsAsc);
					transactions = transactionz.getContent();
				}
			} else {
				// default case - a singular date in the past till today
				// if TransactionType was specified
				if (transactionType != null) {
					Page<TransactionEvent> transactionz = transactionRepository
							.findByAccountAndTransactionTsBetweenAndTransactionType(account, defaultDateTime, now,
									transactionType, transactionEventsSortedByTransactionTsAsc);
					transactions = transactionz.getContent();
				} else {
					Page<TransactionEvent> transactionz = transactionRepository.findByAccountAndTransactionTsBetween(
							account, defaultDateTime, now, transactionEventsSortedByTransactionTsAsc);
					transactions = transactionz.getContent();
				}
			}
		}
		return new ResponseEntity<List<TransactionEvent>>(transactions, HttpStatus.OK);
	}
}