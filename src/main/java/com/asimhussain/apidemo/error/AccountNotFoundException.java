package com.asimhussain.apidemo.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AccountNotFoundException extends Exception {
	
	public AccountNotFoundException(String errorMessage) {
        super(errorMessage);
	}

}
