package com.asimhussain.apidemo.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.asimhussain.apidemo.support.TransactionType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class TransactionEvent {

	@Id @NotNull @GeneratedValue @JsonIgnore private Long transactionId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false) 
	@JoinColumn(name="accountId", nullable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    private Account account;
	
	private BigDecimal amount;
	private LocalDateTime transactionTs;
	@JsonProperty("type") private TransactionType transactionType;

    @JsonIgnore
    public Account getAccount() {
        return account;
    }
    
    //getter method to retrieve the accountId
    @JsonProperty("accountNumber")
    public Long getAccount_id(){
        return account.getId();
    }
	
}