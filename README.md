# API Demo
Implementation example of RESTFUL APIs using Spring Boot designed by *Asim Hussain*


### Description
This API demo is a fictitious example of querying an individual's bank account for their latest balance and also for their transactions

### Technologies used under the hood ;)
- Java 8/11
- Spring Boot
- Restful APIs
- Swagger
- JSON
- Error handling
- JPA
- Hibernate
- H2 in-memory database
- Logging
- Spring services
- Spring configuration
- Dummy data loader
- Custom Exceptions

### Operations
The API can serve the following queries:

1. Given an accountNumber, it returns the latest balance.
2. Given an accountNumber and a time range such as: Today, Last 7 days, last Month and the more general case of a range between date X and date Y, it lists all consecutive transactions. For example, you can query all transactions between January 8th, 2019 to November 28th, 2020 (data dependent as it is randomized. Check the DB for exact time ranges and transactions). NOTE, the date range is a string in the format: YYYY-MM-dd
3. Step 2 can be repeated with a filter type. I.E. Show transactions with type WITHDRAW or DEPOSIT

### Message Format

#### Balance
`
{"accountNumber": "abc", "lastUpdateTimestamp": "2020-01-01T01:02:03.8Z", "balance": 89.1}
`
#### Transaction

Keep in mind that there are two types of transactions: 1. DEPOSIT and 2. WITHDRAW.

For a DEPOSIT:

```
{"accountNumber": "abc", "transactionTs": "2020-01-03T01:02:03.8Z", "type": "DEPOSIT", "amount": 89.1}
```


For WITHDRAW:

```
{"accountNumber": "abc", "transactionTs": "2020-01-03T01:02:03.8Z", "type": "WITHDRAW", "amount": 89.1}
```

### Steps to run
1. Build the project using
  `mvn clean install` (NOTE: You must have Java 11 installed)
2. Run using `mvn spring-boot:run`
3. The REST application should be live now. Try GET <http://localhost:8080/health/alive> You should get a message that says: "I am alive!"
4. See all the APIs you can use by going to Swagger UI: <http://localhost:8080/swagger-ui/index.html>
5. In order for the application to work, dummy data needs to be inserted. Currently, there is data inserted for 100 users who have 30 BalanceEvents and 30 TransactionEvents. This is configurable. See application.properties. **To LOAD dummy data:** POST <http://localhost:8080/data/add>
6. Log into H2 console (default DB) to verify data: <http://localhost:8080/h2-console>
7. Use the data from H2 (in the respective tables) to plug into the APIs.
8. From here, you may test the respective APIs. See <http://localhost:8080/swagger-ui/index.html>