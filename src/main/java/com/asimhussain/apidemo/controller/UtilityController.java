package com.asimhussain.apidemo.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asimhussain.apidemo.support.LoadDataUtil;

import io.swagger.annotations.Api;

@RestController
@Api(tags="Utility Controller")
public class UtilityController {
	
	@Autowired private LoadDataUtil loadDataUtil;

    @GetMapping("/health/alive")
    public String live() {
        return "I am alive!";
    }

    @PostMapping("/data/add")
    public ResponseEntity<HttpStatus> addData() throws ParseException {
    	// dummy data for this demo
    	loadDataUtil.loadDummyData();
    	return new ResponseEntity<>(HttpStatus.OK);
    }
}